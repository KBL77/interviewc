package com.niyasys.interviewcoach.common;

import java.io.Serializable;

/**
 * Package Name: com.niyasys.myinterview.common
 * Author: chandrav
 */
public final class Response implements Serializable
{
	public static final String STATUS_OK = "OK";
	public static final String STATUS_ERROR = "ERROR";

	private String status;
	private Object data;
	private String pageId;

	public Response()
	{
		// Empty constructor
		status = Response.STATUS_OK;
	}

	public Response( String status, Object data )
	{
		this();
		this.status = status;
		this.data = data;
	}

	public String getStatus()
	{
		return status;
	}

	public Response setStatus( final String status )
	{
		this.status = status;
		return this;
	}

	public Object getData()
	{
		return data;
	}

	public Response setData( final Object data )
	{
		this.data = data;
		return this;
	}

	public String getPageId()
	{
		return pageId;
	}

	public Response setPageId( final String pageId )
	{
		this.pageId = pageId;
		return this;
	}
}
