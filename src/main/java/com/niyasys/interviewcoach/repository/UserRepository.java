package com.niyasys.interviewcoach.repository;


import com.niyasys.interviewcoach.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.neo4j.repository.RelationshipOperationsRepository;
import org.springframework.data.repository.query.Param;

/**
 * Created by saibhavani on 30/6/14.
 */
public interface UserRepository extends GraphRepository<User>, RelationshipOperationsRepository<User>, UserRepositoryExtension, MIUserDetailsService
{

    public User findByEmail( String email );

    public User findByObjectUUID( String objectUUID );

    @Query( "match (u:User)-[:`ANSWERED`]->(a:Answer)-[:`FOR`]->(q:Question{objectUUID:{queUUID}}) return u order by a.likes desc" )
    Page<User> getTheUserDetails( @Param( "queUUID" ) String queUUID, Pageable pageable );

    @Query( "match (q:Question{objectUUID:{queUUID}})<-[answer:`POSTED`]-(user) return user" )
    User getUserDetails( @Param( "queUUID" ) String queUUID );

    @Query( "match (a:Answer)<-[comment:`COMMENT`{objectUUID:{commentUUID}}]-(user:User) return user" )
    User userDetails( @Param( "commentUUID" ) String commentUUID );
}