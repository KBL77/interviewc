package com.niyasys.interviewcoach.repository;

import com.niyasys.interviewcoach.model.Answer;
import com.niyasys.interviewcoach.model.User;

import java.util.Map;

/**
 * Created by achiever on 21/7/14.
 */
public interface AnswerRepositoryExtension
{

	String storeAnswerData( String fromUserUUID, String answerName, String questionUUID );

	void buildAnsweredRelationshipBetween( String fromUserUUID, String toAnswerUUID, String questionUUID );

	void buildForRelationshipBetween( String fromAnswerUUID, String toQuestionUUID );

	Map<String, Object> updateLikes( User user, Answer answer );

	Map<String, Object> updateDisLikes( User user, Answer answer );

    Map<String, Object> addComments( User user, Answer answer, String commentDescription );

}
