package com.niyasys.interviewcoach.repository;


import com.niyasys.interviewcoach.model.Question;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.neo4j.repository.RelationshipOperationsRepository;
import org.springframework.data.repository.query.Param;


/**
 * Created by saibhavani on 30/6/14.
 */
public interface QuestionRepository extends GraphRepository<Question>, RelationshipOperationsRepository<Question>, QuestionRepositoryExtension
{

	public Question findByObjectUUID( String objectUUID );

	@Query("MATCH (question:Question) WHERE question.questionDescription =~ {searchQuery} RETURN question")
	Page<Question> searchResults( @Param("searchQuery") String searchQuery, Pageable pageable );

	@Query("MATCH (question:Question{objectUUID:{queUUID}}) SET question.noOfViews={noOfViews}")
	void updateNoOfViews( @Param("queUUID") String queUUID, @Param("noOfViews") int noOfViews );

	@Query("MATCH (question:Question{objectUUID:{queUUID}}) SET question.noOfAnswers={noOfAnswers}")
	void updateNoOfAnswers( @Param("queUUID") String queUUID, @Param("noOfAnswers") int noOfAnswers );

	@Query("MATCH (question:Question) RETURN question ORDER BY question.postingTime DESC")
	Page<Question> findAll( Pageable pageable );

	@Query("MATCH (question:Question) WHERE question.categoryType={categoryType} RETURN question ORDER BY question.postingTime DESC")
	Page<Question> findAllByCategory( @Param("categoryType") String categoryType, Pageable pageable );

	@Query("MATCH (question:Question) WHERE question.categoryType={categoryType} RETURN count(question)")
	int countOfCategoryQuestions( @Param("categoryType") String categoryType );

	@Query("MATCH (question:Question) WHERE question.companyName={companyName} RETURN question ORDER BY question.postingTime DESC")
	Page<Question> findAllByCompany( @Param("companyName") String companyName, Pageable pageable );

    @Query( "match (question:Question)<-[posted:`POSTED`]-(user:User{email:{email}}) return question order by question.postingTime desc")
    Page<Question> getAllTheQuestionsPosted( @Param( "email" ) String email, Pageable pageable );

    @Query( "match (answer:Answer{objectUUID:{answerUUID}})-[for:`FOR`]->(question:Question) return question")
    Question getByUser( @Param( "answerUUID" ) String answerUUID );
}