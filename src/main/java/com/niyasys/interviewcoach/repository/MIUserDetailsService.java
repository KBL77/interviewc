/*
 * Copyright (c) 2014. Niyasys Technologies Pvt Ltd, India. All rights reserved.
 *
 * This software is the confidential and proprietary information of Niyasys Technologies Pvt Ltd.
 */
package com.niyasys.interviewcoach.repository;

import com.niyasys.interviewcoach.model.User;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * Interface for loading user data (wraps the spring User to CDUser)
 *
 * @author Chandra Veerapaneni
 */
public interface MIUserDetailsService extends UserDetailsService
{
	public User getUserFromSession();

}