package com.niyasys.interviewcoach.repository;


import com.niyasys.interviewcoach.model.User;

/**
 * Created by saibhavani on 30/6/14.
 */
public interface UserRepositoryExtension  {

    User storeUserData(User user) throws Exception;

    void buildFollowRelationshipBetween( String fromUserUUID, String toUserUUID );
}
