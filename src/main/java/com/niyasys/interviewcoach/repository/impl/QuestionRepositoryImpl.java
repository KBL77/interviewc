package com.niyasys.interviewcoach.repository.impl;

import com.niyasys.interviewcoach.model.Question;
import com.niyasys.interviewcoach.model.User;
import com.niyasys.interviewcoach.relationships.RelationPosted;
import com.niyasys.interviewcoach.repository.QuestionRepository;
import com.niyasys.interviewcoach.repository.QuestionRepositoryExtension;
import com.niyasys.interviewcoach.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.neo4j.template.Neo4jOperations;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

/**
 * Created by achiever on 4/7/14.
 */
public class QuestionRepositoryImpl implements QuestionRepositoryExtension {

    @Autowired
    QuestionRepository questionRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    Neo4jOperations neo4jOperations;

    @Transactional
    @Override
    public Question storeQuestionData(String fromUserUUID, Question questionData) {

        Question question = new Question();

        String questionUUID = UUID.randomUUID().toString();
        question.setObjectUUID(questionUUID); // Sets Object UUID
        question.setQuestionTitle(questionData.getQuestionTitle()); // Sets Question Title
        question.setQuestionDescription(questionData.getQuestionDescription()); // Sets Question Description
        question.setCompanyName(questionData.getCompanyName()); // Sets Company Name
        question.setCategoryType(questionData.getCategoryType()); // Sets Category Type
        question.setPostingTime(System.currentTimeMillis()); // Sets Posting Time
        question.setNoOfAnswers(0);
        question.setNoOfViews(0);

        try {
            neo4jOperations.save(question);
            System.out.println("Created question in database");
            questionRepository.buildPostedRelationshipBetween(fromUserUUID,questionUUID);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return question;
    }

    @Transactional
    @Override
    public void buildPostedRelationshipBetween(String fromUserUUID, String toQuestionUUID) {

        User user = userRepository.findByObjectUUID(fromUserUUID);
        Question question = questionRepository.findByObjectUUID(toQuestionUUID);
        if( user != null && question != null )
        {
            neo4jOperations.createRelationshipBetween(user, question, RelationPosted.class, RelationPosted.getRelationshipType(),false);
            System.out.println("Created the Posted relationship between the user and question..");
        }
    }
}
