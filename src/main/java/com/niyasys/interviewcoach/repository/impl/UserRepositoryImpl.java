package com.niyasys.interviewcoach.repository.impl;


import com.niyasys.interviewcoach.model.User;
import com.niyasys.interviewcoach.relationships.RelationFollow;
import com.niyasys.interviewcoach.repository.MIUserDetails;
import com.niyasys.interviewcoach.repository.MIUserDetailsService;
import com.niyasys.interviewcoach.repository.UserRepository;
import com.niyasys.interviewcoach.repository.UserRepositoryExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.neo4j.template.Neo4jOperations;
import org.springframework.security.authentication.dao.SaltSource;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.UUID;

/**
 * Created by saibhavani on 30/6/14.
 */
public class UserRepositoryImpl implements UserRepositoryExtension, MIUserDetailsService
{
    @Autowired
    UserRepository userRepository;

    @Autowired
    Neo4jOperations neo4jOperations;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private SaltSource saltSource;

    @Override
    public User getUserFromSession()
    {
        System.out.println("came to get the user");
        final SecurityContext context = SecurityContextHolder.getContext();
        if ( context != null )
        {
            System.out.println("context not null");
            final Authentication authentication = context.getAuthentication();
            if ( authentication != null && authentication.getPrincipal() != null )
            {
                System.out.println("authentication not null");
                final Object principal = authentication.getPrincipal();
                System.out.println("principle "+principal);
                if ( principal instanceof MIUserDetails )
                {
                    System.out.println("and entered into principle services");
                    final MIUserDetails userDetails = (MIUserDetails) principal;
                    return userDetails != null ? userDetails.getUser() : null;
                }
            }
        }
        return null;
    }

    @Override
    public UserDetails loadUserByUsername( final String username ) throws UsernameNotFoundException
    {
        System.out.println("It came");
        String trUserName = StringUtils.hasLength( username ) ? StringUtils.trimWhitespace( username ) : null;
        System.out.println("user name1 : "+trUserName);
        User user = StringUtils.hasLength( trUserName ) ? userRepository.findByEmail( trUserName ) : null;
        System.out.println("founded user "+user);
        if ( user == null )
        {
            // The user does not exist. Throw an exception.
            throw new UsernameNotFoundException( "User account does not exist for the specified email address." );
        }
        System.out.println("found "+user.getEmail());
        // Return a new MIUserDetails() object
        return new MIUserDetails( user );
    }

    @Transactional
    @Override
    public User storeUserData( User user ) throws Exception
    {
        System.out.println( "creating a user data in db" );
        User uu = new User();

        String emailID = user.getEmail();
        if(user.getDisplayName() == null)
        {
            String[] screenName = emailID.split("@");
            uu.setDisplayName(screenName[0]);
            System.out.println("screen name is : "+screenName[0]);
        }

        if(user.getPassword() != null){
            uu.setPassword( getEncodedPassword( user.getPassword(), user ) );
        }

        uu.setObjectUUID( UUID.randomUUID().toString() );
        uu.setEmail(user.getEmail());
//        uu.setDisplayName(user.getDisplayName());
        uu.setGender(user.getGender());
        uu.setFirstName(user.getFirstName());
        uu.setLastName(user.getLastName());

        try
        {
            user = neo4jOperations.save( uu );
            System.out.println( "Created user in DB " + user.getEmail() );
        }
        catch ( Exception e )
        {
            System.out.println( e.getMessage() );
        }
        return user;
    }

    @Transactional
    @Override
    public void buildFollowRelationshipBetween( String fromUserUUID, String toUserUUID )
    {
        User u1 = userRepository.findByObjectUUID( fromUserUUID );
        User u2 = userRepository.findByObjectUUID( toUserUUID );
        if ( u1 != null && u2 != null )
        {
            neo4jOperations.createRelationshipBetween( u1, u2, RelationFollow.class, RelationFollow.getRelationshipType(), false );
            System.out.println( "Created the follow relationship between the 2 users    " );
        }
    }

    /**
     *
     * @param plainTextPassword
     * @param user
     * @return
     * @throws Exception
     */
    private String getEncodedPassword( String plainTextPassword, User user ) throws Exception
    {
        return passwordEncoder.encodePassword( plainTextPassword, saltSource.getSalt( new MIUserDetails( user ) ) );
    }

}
