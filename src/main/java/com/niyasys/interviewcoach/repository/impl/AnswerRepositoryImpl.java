package com.niyasys.interviewcoach.repository.impl;

import com.niyasys.interviewcoach.model.Answer;
import com.niyasys.interviewcoach.model.Question;
import com.niyasys.interviewcoach.model.User;
import com.niyasys.interviewcoach.relationships.*;
import com.niyasys.interviewcoach.repository.AnswerRepository;
import com.niyasys.interviewcoach.repository.AnswerRepositoryExtension;
import com.niyasys.interviewcoach.repository.QuestionRepository;
import com.niyasys.interviewcoach.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.neo4j.template.Neo4jOperations;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by achiever on 21/7/14.
 */
public class AnswerRepositoryImpl implements AnswerRepositoryExtension
{

	@Autowired
	Neo4jOperations neo4jOperations;

	@Autowired
	AnswerRepository answerRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	QuestionRepository questionRepository;

	@Transactional
	@Override
	public String storeAnswerData( String fromUserUUID, String answerName, String questionUUID )
	{

		System.out.println( "Inside storing answer data and questionUUID is " + questionUUID );

		Answer answered = answerRepository.isAnswered( fromUserUUID, questionUUID );

		if ( answered == null )
		{
			Answer answer = new Answer();

			String answerUUID = UUID.randomUUID().toString();
			answer.setObjectUUID( answerUUID );
			answer.setAnswerName( answerName );
			answer.setLikes( 0 );
			answer.setDisLikes( 0 );
			answer.setCurrentTime( System.currentTimeMillis() );

			System.out.println( "just above saving" );
			try
			{
				neo4jOperations.save( answer );
				System.out.println( "Answer data stored in database" );
				answerRepository.buildAnsweredRelationshipBetween( fromUserUUID, answerUUID, questionUUID );
			}
			catch ( Exception e )
			{
				System.out.println( e.getMessage() );
			}

			return "true";
		}

		else
		{
			System.out.println( "sorry, u already answered for this question" );
			return "false";
		}
	}

	@Transactional
	@Override
	public void buildAnsweredRelationshipBetween( String fromUserUUID, String toAnswerUUID, String questionUUID )
	{

		System.out.println( "creating answered relationship between user and answer and " + questionUUID );

		User user = userRepository.findByObjectUUID( fromUserUUID );
		Answer answer = answerRepository.findByObjectUUID( toAnswerUUID );

		if ( user != null && answer != null )
		{
			neo4jOperations.createRelationshipBetween( user, answer, RelationAnswered.class, RelationAnswered.getRelationshipType(), false );
			System.out.println( "created relationship between user and answer as ANSWERED and " + questionUUID );
			answerRepository.buildForRelationshipBetween( toAnswerUUID, questionUUID );
		}
	}

	@Transactional
	@Override
	public void buildForRelationshipBetween( String fromAnswerUUID, String toQuestionUUID )
	{

		System.out.println( "creating for relationship between answer and question" );

		Answer fromAnswer = answerRepository.findByObjectUUID( fromAnswerUUID );
		Question toQuestion = questionRepository.findByObjectUUID( toQuestionUUID );

		System.out.println( fromAnswer + " and " + toQuestionUUID );

		if ( toQuestion != null )
		{
			neo4jOperations.createRelationshipBetween( fromAnswer, toQuestion, RelationFor.class, RelationFor.getRelationshipType(), false );
			System.out.println( "created for relationship between answer and question" );

			System.out.println( "increasing no of answers by 1" );
			questionRepository.updateNoOfAnswers( toQuestion.getObjectUUID(), toQuestion.getNoOfAnswers() + 1 );
			System.out.println( "updated no of answers by increasing 1" );
		}
	}

	@Transactional
	@Override
	public Map<String, Object> updateLikes( User user, Answer answer )
	{
		System.out.println( user + " and " + answer );
		RelationLiked relationLiked = neo4jOperations.getRelationshipBetween( user, answer, RelationLiked.class, RelationLiked.getRelationshipType() );

        Map<String, Object> votes = new HashMap<>();

		int noOfLikes = answer.getLikes();
		int noOfDisLikes = answer.getDisLikes();

		String answerUUID = answer.getObjectUUID();

		if ( relationLiked == null )
		{

			answerRepository.updateNoOfLikes( answerUUID, noOfLikes + 1 );
			System.out.println( "updated likes from " + noOfLikes + " to " + ( noOfLikes + 1 ) );

			neo4jOperations.createRelationshipBetween( user, answer, RelationLiked.class, RelationLiked.getRelationshipType(), false );
			neo4jOperations.deleteRelationshipBetween( user, answer, RelationDisLiked.getRelationshipType() );

			votes.put( "noOfLikes", ( noOfLikes + 1 ) - noOfDisLikes );
			votes.put( "isUpdated", "true" );
			return votes;
		}
		votes.put( "noOfLikes", noOfLikes - noOfDisLikes );
		votes.put( "isUpdated", "false" );
		System.out.println( "u already liked, so u can't like now" );
		return votes;
	}

	@Transactional
	@Override
	public Map<String, Object> updateDisLikes( User user, Answer answer )
	{
        System.out.println( user + " and " + answer );
		RelationDisLiked relationDisLiked = neo4jOperations.getRelationshipBetween( user, answer, RelationDisLiked.class, RelationDisLiked.getRelationshipType() );

		Map<String, Object> votes = new HashMap<>();

		int noOfLikes = answer.getLikes();
		int noOfDisLikes = answer.getDisLikes();

		String answerUUID = answer.getObjectUUID();

		if ( relationDisLiked == null )
		{

			answerRepository.updateNoOfDisLikes( answerUUID, noOfDisLikes + 1 );
			System.out.println( "updated likes from " + noOfLikes + " to " + ( noOfLikes - 1 ) );

			neo4jOperations.createRelationshipBetween( user, answer, RelationDisLiked.class, RelationDisLiked.getRelationshipType(), false );
			neo4jOperations.deleteRelationshipBetween( user, answer, RelationLiked.getRelationshipType() );

			votes.put( "noOfLikes", noOfLikes - ( noOfDisLikes + 1 ) );
			votes.put( "isUpdated", "true" );

			return votes;
		}
		votes.put( "noOfLikes", noOfLikes - noOfDisLikes );
		votes.put( "isUpdated", "false" );
		System.out.println( "u already disliked, so u can't able to unlike until u like" );
		return votes;
	}

    @Transactional
    @Override
    public Map<String, Object> addComments(User user, Answer answer, String commentDescription) {
        Map<String, Object> comments = new HashMap<>();

        System.out.println("Adding comments");

        RelationComment relationComment = neo4jOperations.createRelationshipBetween(user, answer, RelationComment.class, RelationComment.getRelationshipType(), true);
        relationComment.setObjectUUID(UUID.randomUUID().toString());
        relationComment.setCommentDescription(commentDescription);
        relationComment.setTimeOfComment(System.currentTimeMillis());
        neo4jOperations.save(relationComment);

        System.out.println("Added comment");

        return comments;
    }
}
