package com.niyasys.interviewcoach.repository;

import com.niyasys.interviewcoach.model.Answer;
import com.niyasys.interviewcoach.relationships.RelationComment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.neo4j.repository.RelationshipOperationsRepository;
import org.springframework.data.repository.query.Param;

/**
 * Created by achiever on 21/7/14.
 */
public interface AnswerRepository extends GraphRepository<Answer>, RelationshipOperationsRepository<Answer>, AnswerRepositoryExtension
{

	@Query( "match (q:Question{objectUUID:{queUUID}})<-[ans:`FOR`]-(answer:Answer) return answer order by answer.likes desc" )
	Page<Answer> getAllTheAnswers( @Param( "queUUID" ) String queUUID, Pageable pageable );

    @Query( "match (answer:Answer)<-[ans:`ANSWERED`]-(user:User{email:{email}}) return answer order by answer.currentTime desc" )
    Page<Answer> getAllTheUserAnswers( @Param( "email" ) String email, Pageable pageable );

	public Answer findByObjectUUID( String objectUUID );

	@Query( "match (u:User{objectUUID:{userUUID}})-[:`ANSWERED`]->(a:Answer)-[:`FOR`]->(q:Question{objectUUID:{questionUUID}}) return a" )
	Answer isAnswered( @Param( "userUUID" ) String userUUID, @Param( "questionUUID" ) String questionUUID );

	@Query( "MATCH (answer:Answer{objectUUID:{ansUUID}}) set answer.likes={noOfLikes}" )
	void updateNoOfLikes( @Param( "ansUUID" ) String ansUUID, @Param( "noOfLikes" ) int noOfLikes );

	@Query( "MATCH (answer:Answer{objectUUID:{ansUUID}}) set answer.disLikes={noOfDisLikes}" )
	void updateNoOfDisLikes( @Param( "ansUUID" ) String ansUUID, @Param( "noOfDisLikes" ) int noOfDisLikes );

    @Query( "match (a:Answer{objectUUID:{ansUUID}})<-[comments:`COMMENT`]-(user) return comments order by comments.timeOfComments desc" )
    Page<RelationComment> getAllComments( @Param("ansUUID") String ansUUID, Pageable pageable);

}
