/*
 * Copyright (c) 2014. Niyasys Technologies Pvt Ltd, India. All rights reserved.
 *
 * This software is the confidential and proprietary information of Niyasys Technologies Pvt Ltd.
 */
package com.niyasys.interviewcoach.repository;

import com.niyasys.interviewcoach.model.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * This class provides user information encapsulated within a User object.
 *
 * @author Chandra Veerapaneni
 */
public class MIUserDetails implements UserDetails
{
	private static final long serialVersionUID = 177633127773269469L;

	public static final String ROLE_USER = "USER";
	public static final String ROLE_ADMIN = "ADMIN";
	public static final String ROLE_ANONYMOUS = "ANONYMOUS";

	private final User user;

	/**
	 * @param user
	 * 		User object
	 */
	public MIUserDetails( User user )
	{
		this.user = user;
        System.out.println("user he is "+user);
    }

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities()
	{
		final List<GrantedAuthority> authList = new ArrayList<>();
		authList.add( new SimpleGrantedAuthority( MIUserDetails.ROLE_ANONYMOUS ) );
        System.out.println("entered to take permissions "+user.getEmail());
        if ( user != null )
		{
			authList.add( new SimpleGrantedAuthority( MIUserDetails.ROLE_USER ) );
		}
		return authList;
	}

	@Override
	public String getPassword()
	{
		return user.getPassword();
	}

	@Override
	public String getUsername()
	{
        return user.getEmail();
	}

	@Override
	public boolean isAccountNonExpired()
	{
		return isAccountNonLocked();
	}

	@Override
	public boolean isAccountNonLocked()
	{
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired()
	{
		return isAccountNonLocked();
	}

	@Override
	public boolean isEnabled()
	{
		return isAccountNonLocked();
	}

	/**
	 * This method returns the user model object wrapped by this UserDetails object
	 *
	 * @return User
	 */
	public User getUser()
	{
		return user;
	}
}
