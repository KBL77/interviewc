package com.niyasys.interviewcoach.repository;

import com.niyasys.interviewcoach.model.Question;

/**
 * Created by achiever on 4/7/14.
 */
public interface QuestionRepositoryExtension {

    Question storeQuestionData(String fromUserUUID, Question questionData);

    void buildPostedRelationshipBetween( String fromUserUUID, String toQuestionUUID );

}
