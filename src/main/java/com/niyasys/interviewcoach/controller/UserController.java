package com.niyasys.interviewcoach.controller;

import com.niyasys.interviewcoach.common.FBUser;
import com.niyasys.interviewcoach.common.Response;
import com.niyasys.interviewcoach.model.User;
import com.niyasys.interviewcoach.repository.QuestionRepository;
import com.niyasys.interviewcoach.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/user")
public class UserController{

    @Autowired
    UserRepository userRepository;

    @Autowired
    QuestionRepository questionRepository;

    @RequestMapping( produces = {"application/json"} )
    public
    @ResponseBody
    Response getUserHomePage() {
        User user = userRepository.getUserFromSession();
        return new Response().setStatus(user != null ? Response.STATUS_OK : Response.STATUS_ERROR).setPageId(user != null ? "suhp" : "sip").setData(null).setData("user:"+user.getDisplayName());
    }

    /**
     * @param user
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/signup", method = RequestMethod.POST, produces = {"application/json"}, consumes = {"application/json"})
    public
    @ResponseBody
    Response signUp(@RequestBody User user) throws Exception {
        Response response = new Response();
        String emailID = user != null ? user.getEmail() : null;
        if (StringUtils.hasLength(emailID)) {
            User u = userRepository.findByEmail(emailID);
            if (u == null) {
                userRepository.storeUserData(user);
                response.setData("Your account has been created.");
            } else {
                response.setStatus(Response.STATUS_ERROR);
                response.setData("Specified email already exists.");
            }
        }
        return response;
    }

    @RequestMapping(value = "/fbuser", method = RequestMethod.POST, produces = {"application/json"}, consumes = {"application/json"})
    public
    @ResponseBody
    Map<String, Object> isUserExist(@RequestBody User user) throws Exception {

        Map<String, Object> results = new HashMap<>();

        System.out.println("is user exist\n");
        String emailID = user !=null ? user.getEmail() : null;
        User u = userRepository.findByEmail(emailID);
//        boolean isFBUserExists = fbUserRepository.isFBUserExists(emailID);
        System.out.println("is user exist\n");
        if(u == null /* && isFBUserExists == false*/)
        {
            System.out.println("if condition\n");
            u = userRepository.storeUserData(user);
//            fbUserRepository.storeFacebookUserData(facebookUser);
        }
        System.out.println("returning " + u.getDisplayName() + "\n");
        results.put("pageID", "suhp");
        results.put("username", u.getDisplayName());
        FBUser.activeFBUser = u;

        System.out.println(FBUser.activeFBUser);
        /*userRepository.loadUserByUsername(u.getEmail());
        User user1 = userRepository.getUserFromSession();
        System.out.println("user1 is"+user1);
        *//*if(user1!=null) {
            System.out.println("not null");
            System.out.println("facebook user added to session : " + user1.getDisplayName());
        }*/
        return results;
    }

    @RequestMapping(value = "/closeUserSession", method = RequestMethod.DELETE)
    public
    @ResponseBody
    void closeUserSession() throws Exception {
        FBUser.activeFBUser = null;
        SecurityContextHolder.getContext().setAuthentication(null);
        User user1 = userRepository.getUserFromSession();
        if(user1 == null){
            System.out.println("user deleted");
        }
    }

}