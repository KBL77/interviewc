package com.niyasys.interviewcoach.controller;

import com.niyasys.interviewcoach.common.FBUser;
import com.niyasys.interviewcoach.model.Answer;
import com.niyasys.interviewcoach.model.Question;
import com.niyasys.interviewcoach.model.User;
import com.niyasys.interviewcoach.relationships.RelationComment;
import com.niyasys.interviewcoach.repository.AnswerRepository;
import com.niyasys.interviewcoach.repository.QuestionRepository;
import com.niyasys.interviewcoach.repository.UserRepository;
import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.neo4j.template.Neo4jOperations;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * Created by achiever on 21/7/14.
 */

@Controller
@RequestMapping( "/questions" )
public class AnswerController
{

    @Autowired
    QuestionRepository questionRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    AnswerRepository answerRepository;

    @Autowired
    Neo4jOperations neo4jOperations;

    @Transactional
    @RequestMapping( value = "/{questionUUID}", method = RequestMethod.GET, produces = {"application/json"} )
    public
    @ResponseBody
    Map<String, Object> getAnswers( @PathVariable( "questionUUID" ) String questionUUID, @RequestParam( "pageNumber" ) int pageNumber )
    {
        JSONArray userOfComments = new JSONArray();
        JSONArray comments = new JSONArray();

        Question qtn = questionRepository.findByObjectUUID( questionUUID );
        User userOfQuestion = userRepository.getUserDetails( questionUUID );

        questionRepository.updateNoOfViews( questionUUID, qtn.getNoOfViews() + 1 );

        Page<Answer> answersInPages = answerRepository.getAllTheAnswers( questionUUID, new PageRequest( pageNumber, 10 ) );
        System.out.println( "Answers for the Question : "+qtn.getQuestionTitle());

        for ( Iterator<Answer> i = answersInPages.iterator(); i.hasNext(); )
        {
            Answer answer = i.next();
            Page<RelationComment> relationComments = answerRepository.getAllComments(answer.getObjectUUID(), new PageRequest( 0 , 10));
            comments.add( relationComments != null ? relationComments.getContent() : null );

            for ( Iterator<RelationComment> j = relationComments.iterator(); j.hasNext(); )
            {
                RelationComment relationComment = j.next();
                User user = userRepository.userDetails(relationComment.getObjectUUID());
                userOfComments.add(user.getDisplayName());
            }
        }

        System.out.println("here came");
        Page<User> usersOfAnswers = userRepository.getTheUserDetails( questionUUID, new PageRequest( pageNumber, 10 ) );

        Map<String, Object> allAnswers = new HashMap<>();

        allAnswers.put( "question", qtn );
        allAnswers.put( "userOfQuestion", userOfQuestion.getDisplayName() );
        allAnswers.put( "totalAnswers", answersInPages.getTotalElements() );
        System.out.println(answersInPages.getTotalElements());
        allAnswers.put( "answers", answersInPages != null ? answersInPages.getContent() : null );
        allAnswers.put( "users", usersOfAnswers != null ? usersOfAnswers.getContent() : null );
        allAnswers.put( "pageNumber", pageNumber );
        allAnswers.put("comments",comments);
        allAnswers.put("usersOfComments",userOfComments);

        return allAnswers;
    }


    @Transactional
    @RequestMapping( value = "/userAnswers", method = RequestMethod.GET, produces = {"application/json"} )
    public
    @ResponseBody
    Map<String, Object> getUserAnswers( @RequestParam( "pageNumber" ) int pageNumber )
    {
        User userFromSession = userRepository.getUserFromSession();

        if(userFromSession == null){
            userFromSession = FBUser.activeFBUser;
        }

        Question question = null;

        Page<Answer> answersInPages = answerRepository.getAllTheUserAnswers(userFromSession.getEmail(), new PageRequest(pageNumber, 3));
        List<Question> questionList = new ArrayList<Question>();

        for ( Iterator<Answer> i = answersInPages.iterator(); i.hasNext(); )
        {
            Answer answer = i.next();
            question = questionRepository.getByUser( answer.getObjectUUID() );
            questionList.add(question);
        }
        System.out.println("here came");
        Map<String, Object> allAnswers = new HashMap<>();

        allAnswers.put( "questions", questionList != null ? questionList : null );
        allAnswers.put( "totalAnswers", answersInPages.getTotalElements() );
        allAnswers.put( "answers", answersInPages != null ? answersInPages.getContent() : null );
        allAnswers.put( "pageNumber", pageNumber );

        return allAnswers;
    }

    @RequestMapping( value = "/{questionUUID}/newAnswer", method = RequestMethod.POST, produces = {"application/json"}, consumes = {"application/json"} )
    public
    @ResponseBody
    Map<String, Object> createAnsweredRelationship( @RequestBody Answer answer, @PathVariable( "questionUUID" ) String questionUUID )
    {
        System.out.println( "This is new answer to be posted" );

        Map<String, Object> results = new HashMap<>();
        System.out.println( "question UUID is " + questionUUID );

        User user = userRepository.getUserFromSession();

        if(user == null){
            user = FBUser.activeFBUser;
        }

        String isSubmitted = answerRepository.storeAnswerData( user.getObjectUUID(), answer.getAnswerName(), questionUUID );

        results.put( "isSubmit", isSubmitted );

        return results;
    }

    @RequestMapping( value = "/{questionUUID}/{answerUUID}/voteUp", method = RequestMethod.GET, produces = {"application/json"} )
    public
    @ResponseBody
    Map<String, Object> voteUp( @PathVariable( "answerUUID" ) String answerUUID )
    {
        System.out.println( "here to set likes" );

        Map<String, Object> votes = new HashMap<>();
        User user = userRepository.getUserFromSession();

        if(user == null){
            user = FBUser.activeFBUser;
        }

        Answer answer = answerRepository.findByObjectUUID( answerUUID );

        votes = answerRepository.updateLikes( user, answer );

        return votes;
    }

    @RequestMapping( value = "/{questionUUID}/{answerUUID}/voteDown", method = RequestMethod.GET, produces = {"application/json"} )
    public
    @ResponseBody
    Map<String, Object> voteDown( @PathVariable( "answerUUID" ) String answerUUID )
    {
        System.out.println( "here to set likes" );


        Map<String, Object> votes = new HashMap<>();

        User user = userRepository.getUserFromSession();

        if(user == null){
            user = FBUser.activeFBUser;
        }

        Answer answer = answerRepository.findByObjectUUID( answerUUID );

        votes = answerRepository.updateDisLikes( user, answer );

        return votes;
    }

    @RequestMapping( value = "/{questionUUID}/{answerUUID}/addComment", method = RequestMethod.POST, produces = {"application/json"}, consumes = {"application/json"})
    public
    @ResponseBody
    Map<String, Object> addComment( @RequestBody RelationComment relationComment, @PathVariable( "answerUUID" ) String answerUUID )
    {
        System.out.println( "here to add comment" );

        Map<String, Object> comments = new HashMap<>();

        String commentDescription = relationComment.getCommentDescription();

        User user = userRepository.getUserFromSession();

        if(user == null){
            user = FBUser.activeFBUser;
        }

        Answer answer = answerRepository.findByObjectUUID( answerUUID );

        comments = answerRepository.addComments(user, answer, commentDescription);

        return comments;
    }
}
