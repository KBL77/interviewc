package com.niyasys.interviewcoach.controller;

import com.niyasys.interviewcoach.common.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Locale;

/**
 * Package Name: com.niyasys.interviewcoach.controller
 * Author: chandrav
 */
@Controller
@RequestMapping( "/auth" )
public class AuthenticationController {
    @Autowired
    private MessageSource messageSource;

    public AuthenticationController() {
        System.out.println("Inside Authentication Controller");
    }

    @RequestMapping(value = "/login", produces = {"application/json"})
    public
    @ResponseBody
    Response getLoginPage() {
        return new Response().setStatus(Response.STATUS_OK).setPageId("sip").setData(null);
    }

    @RequestMapping(value = "/logout", produces = {"application/json"})
    public
    @ResponseBody
    Response getLogoutPage() {
        return new Response().setStatus(Response.STATUS_OK).setPageId("uhp").setData(null).setData("Invalid Credentials");
    }

    @RequestMapping(value = "/login-failed", produces = {"application/json"})
    public
    @ResponseBody
    Response getFailedLoginPage(Locale locale) {
        return new Response().setStatus(Response.STATUS_ERROR).setData(null).setData(messageSource.getMessage("invalidCredentials.MESSAGE", null, locale)).setPageId("sip");
    }
}