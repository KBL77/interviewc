package com.niyasys.interviewcoach.controller;

import com.niyasys.interviewcoach.common.FBUser;
import com.niyasys.interviewcoach.common.Response;
import com.niyasys.interviewcoach.model.Question;
import com.niyasys.interviewcoach.model.User;
import com.niyasys.interviewcoach.repository.QuestionRepository;
import com.niyasys.interviewcoach.repository.UserRepository;
import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.neo4j.template.Neo4jOperations;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@Controller
@RequestMapping( "/questions" )
public class QuestionController
{

    @Autowired
    QuestionRepository questionRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    Neo4jOperations neo4jOperations;

    public QuestionController()
    {
        System.out.println( "Inside QuestionController" );
    }

    @RequestMapping( value = "", method = RequestMethod.GET, produces = {"application/json"} )
    public
    @ResponseBody
    Map<String, Object> getQuestions( @RequestParam( "pageNo" ) int pageNo, @RequestParam( "categoryType" ) String categoryType, @RequestParam( "companyName" ) String companyName )
    {
        long totalQuestionsSize = questionRepository.count();

        Map<String, Object> results = new HashMap<>();

        List<User> usersOfQuestions = new ArrayList<>();

        Pageable pageable = new PageRequest( pageNo, 10 );
        Page<Question> questions;

        if( categoryType.equalsIgnoreCase("") && companyName.equalsIgnoreCase("") )
        {
            questions = questionRepository.findAll(pageable);
        }
        else if( companyName.equalsIgnoreCase("") )
        {
            questions = questionRepository.findAllByCategory( categoryType, pageable );
            totalQuestionsSize = questions.getTotalElements();
        }
        else
        {
            questions = questionRepository.findAllByCompany( companyName, pageable );
            totalQuestionsSize = questions.getTotalElements();
        }

        for ( Iterator<Question> i = questions.iterator(); i.hasNext(); )
        {
            Question question = i.next();
            String questionsUUID = question.getObjectUUID();

            User user = userRepository.getUserDetails( questionsUUID );
            usersOfQuestions.add( user );
        }

        results.put( "totalQuestions", totalQuestionsSize );
        results.put( "pageNum", pageNo );
        results.put( "questions", questions != null ? questions.getContent() : null );
        results.put( "users", usersOfQuestions );

        return results;
    }

    @RequestMapping( value = "/count", method = RequestMethod.GET, produces = {"application/json"} )
    public
    @ResponseBody
    Map<String, Object> getCountOfQuestions()
    {
        Map<String, Object> noOfQuestionsForTag = new HashMap<>();
        JSONArray arrayForCount = new JSONArray();

        List<String> categories = Arrays.asList("Aptitude","General","Grammar","IT","Maths","Verbal");

        for( String s : categories )
        {
            arrayForCount.add(questionRepository.countOfCategoryQuestions(s));
        }

        noOfQuestionsForTag.put("tagSize", arrayForCount);

        return noOfQuestionsForTag;
    }

    @RequestMapping( value = "/newQuestion", method = RequestMethod.POST, produces = {"application/json"}, consumes = {"application/json"} )
    public
    @ResponseBody
    Response findUser2( @RequestBody Question q)
    {
        User user = userRepository.getUserFromSession();

        if(user == null){
            user = FBUser.activeFBUser;
        }

        System.out.println("Session is active with the user : "+user.getEmail());
        questionRepository.storeQuestionData(user.getObjectUUID(), q);
        return new Response().setStatus(Response.STATUS_OK).setData("Ok").setPageId("questionsPage");
    }

    @RequestMapping(value="/userQuestions", method = RequestMethod.GET, produces = {"application/json"})
    public
    @ResponseBody
    Map<String, Object> getAllUpdates( @RequestParam( "pageNo" ) int pageNo )
    {
        User user = userRepository.getUserFromSession();
        if(user == null){
            user = FBUser.activeFBUser;
        }

        System.out.println("active user is "+user);
        Map<String, Object> allTheQuestions = new HashMap<>();
        Pageable pageable = new PageRequest( pageNo, 3 );
        Page<Question> questions = questionRepository.getAllTheQuestionsPosted( user.getEmail(), pageable );

        allTheQuestions.put( "questions", questions != null ? questions.getContent() : null );
        allTheQuestions.put( "totalQuestions", questions.getTotalElements() );
        allTheQuestions.put( "pageNum", pageNo );

        return allTheQuestions;
    }
}