package com.niyasys.interviewcoach.controller;

import com.niyasys.interviewcoach.model.Question;
import com.niyasys.interviewcoach.model.User;
import com.niyasys.interviewcoach.repository.QuestionRepository;
import com.niyasys.interviewcoach.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping("/search")
public class SearchController
{
    @Autowired
    QuestionRepository questionRepository;

    @Autowired
    UserRepository userRepository;

    @RequestMapping(method = RequestMethod.GET, produces = {"application/json"})
    public
    @ResponseBody
    Map<String, Object> searchResult( @RequestParam("searchText") String searchText, @RequestParam(value = "pageNo", required = false) Integer pageNo, @RequestParam( value = "limit", required = false ) Integer limit )
    {
        Map<String, Object> results = new HashMap<>();
        String trimmedTxt = StringUtils.hasLength( searchText ) ? StringUtils.trimWhitespace( searchText ) : null;
        String searchString = StringUtils.hasLength( trimmedTxt ) ? "(?i).*" + trimmedTxt.replaceAll( "[\\s]+", ".*" ) + ".*" : ".*";

        System.out.println("search string : "+searchString);


        int currentPageNo = pageNo == null ? 0 : pageNo;
        int pageLimit = limit == null ? 10 : limit;
        Page<Question> questions = questionRepository.searchResults( searchString, new PageRequest( currentPageNo, pageLimit ) );
        List<User> usersOfQuestions = new ArrayList<>();

        for ( Iterator<Question> i = questions.iterator(); i.hasNext(); )
        {
            Question question = i.next();
            String questionsUUID = question.getObjectUUID();

            User user = userRepository.getUserDetails( questionsUUID );
            usersOfQuestions.add( user );
        }

        results.put( "totalResults", questions.getTotalElements() );
        results.put( "pageNum", pageNo );
        results.put( "results", questions != null ? questions.getContent() : null );
        results.put( "users", usersOfQuestions );
        System.out.println(results);
        System.out.println(usersOfQuestions);
        return results;
    }

}