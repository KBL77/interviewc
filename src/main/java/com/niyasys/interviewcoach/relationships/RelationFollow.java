package com.niyasys.interviewcoach.relationships;

import com.niyasys.interviewcoach.model.User;
import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

/**
 * Created by achiever on 3/7/14.
 */

@RelationshipEntity(type = "FOLLOWS")
public class RelationFollow {

    @GraphId
    private Long id;

    @StartNode
    private User fromUser;

    @EndNode
    private User toUser;

    public static String getRelationshipType()
    {
        return "FOLLOWS";
    }

    public User getFromUser()
    {
        return fromUser;
    }

    public void setFromUser(User fromUser)
    {
        this.fromUser = fromUser;
    }

    public User getToUser()
    {
        return toUser;
    }

    public void setToUser(User toUser)
    {
        this.toUser = toUser;
    }

}
