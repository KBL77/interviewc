package com.niyasys.interviewcoach.relationships;

import com.niyasys.interviewcoach.model.Answer;
import com.niyasys.interviewcoach.model.Question;
import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

/**
 * Created by achiever on 21/7/14.
 */

@RelationshipEntity(type = "FOR")
public class RelationFor {

    @GraphId
    private Long id;

    @StartNode
    private Answer startNode;

    public Question getEndNode() {
        return endNode;
    }

    public void setEndNode(Question endNode) {
        this.endNode = endNode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Answer getStartNode() {
        return startNode;
    }

    public void setStartNode(Answer startNode) {
        this.startNode = startNode;
    }

    @EndNode
    private Question endNode;

    public static String getRelationshipType()
    {
        return "FOR";
    }

}
