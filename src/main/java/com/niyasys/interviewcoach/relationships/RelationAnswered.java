package com.niyasys.interviewcoach.relationships;

import com.niyasys.interviewcoach.model.Answer;
import com.niyasys.interviewcoach.model.User;
import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

/**
 * Created by achiever on 3/7/14.
 */

@RelationshipEntity(type = "ANSWERED")
public class RelationAnswered {

    @GraphId
    private Long id;

/*    @Indexed(unique = true)
    private String objectUUID;

    public String getObjectUUID() {
        return objectUUID;
    }

    public void setObjectUUID(String objectUUID) {
        this.objectUUID = objectUUID;
    }*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @StartNode
    private User startNode;

    @EndNode
    private Answer endNode;


/*    @Indexed
    private String answerName;

    @Indexed
    private long currentTime;

    @Indexed
    private int likes;

    @Indexed
    private int disLikes;*/


    public static String getRelationshipType()
    {
        return "ANSWERED";
    }

    public User getStartNode()
    {
        return startNode;
    }

    public void setStartNode(User startNode)
    {
        this.startNode = startNode;
    }

    public Answer getEndNode()
    {
        return endNode;
    }

    public void setEndNode(Answer endNode)
    {
        this.endNode = endNode;
    }

}
