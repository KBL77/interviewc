package com.niyasys.interviewcoach.relationships;

import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.RelationshipEntity;

/**
 * Created by achiever on 18/7/14.
 */

@RelationshipEntity(type = "COMMENT")
public class RelationComment {

    @GraphId
    private Long id;

    @Indexed(unique = true)
    private String objectUUID;

    @Indexed
    private long timeOfComment;

    public String getCommentDescription() {
        return commentDescription;
    }

    public void setCommentDescription(String commentDescription) {
        this.commentDescription = commentDescription;
    }

    private String commentDescription;

    public String getObjectUUID() {
        return objectUUID;
    }

    public void setObjectUUID(String objectUUID) {
        this.objectUUID = objectUUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getTimeOfComment() {
        return timeOfComment;
    }

    public void setTimeOfComment(long timeOfLike) {
        this.timeOfComment = timeOfLike;
    }

    public static String getRelationshipType()
    {
        return "COMMENT";
    }
}
