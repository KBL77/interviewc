package com.niyasys.interviewcoach.relationships;

import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.RelationshipEntity;

/**
 * Created by achiever on 18/7/14.
 */

@RelationshipEntity(type = "LIKED")
public class RelationLiked {

    @GraphId
    private Long id;

    @Indexed(unique = true)
    private String objectUUID;

    @Indexed
    private long timeOfLike;

    public String getObjectUUID() {
        return objectUUID;
    }

    public void setObjectUUID(String objectUUID) {
        this.objectUUID = objectUUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getTimeOfLike() {
        return timeOfLike;
    }

    public void setTimeOfLike(long timeOfLike) {
        this.timeOfLike = timeOfLike;
    }

    public static String getRelationshipType()
    {
        return "LIKED";
    }
}
