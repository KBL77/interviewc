package com.niyasys.interviewcoach.relationships;

import com.niyasys.interviewcoach.model.Question;
import com.niyasys.interviewcoach.model.User;
import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

/**
 * Created by achiever on 3/7/14.
 */

@RelationshipEntity(type = "POSTED")
public class RelationPosted {

    @GraphId
    private Long id;

    @StartNode
    private User startNode;

    @EndNode
    private Question endNode;

    public static String getRelationshipType()
    {
        return "POSTED";
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public User getStartNode()
    {
        return startNode;
    }

    public void setStartNode(User startNode)
    {
        this.startNode = startNode;
    }

    public Question getEndNode()
    {
        return endNode;
    }

    public void setEndNode(Question endNode)
    {
        this.endNode = endNode;
    }
}
