package com.niyasys.interviewcoach.model;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.neo4j.annotation.Indexed;

/**
 * Created by achiever on 21/7/14.
 */

@TypeAlias(value = "Answer")
public class Answer extends AbstractEntity {

    @Indexed
    private String answerName;

    @Indexed
    private long currentTime;

    @Indexed
    private int likes;

    @Indexed
    private int disLikes;

    public String getAnswerName() {
        return answerName;
    }

    public void setAnswerName(String answerName) {
        this.answerName = answerName;
    }

    public long getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(long currentTime) {
        this.currentTime = currentTime;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public int getDisLikes() {
        return disLikes;
    }

    public void setDisLikes(int disLikes) {
        this.disLikes = disLikes;
    }
}
