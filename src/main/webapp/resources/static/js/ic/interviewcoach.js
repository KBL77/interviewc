/**
 * Created by achiever on 14/7/14.
 */


(function ( $ ) {

    var connectionStatus = false;
    var userCount = 0;
    var loginUser = null;
    $("#previousQuestion").hide();
    $("#nextQuestion").hide();
    $("#prvsQuestion").hide();
    $("#nxtQuestion").hide();
    $("#nxtAnswer").hide();
    $("#prvsAnswer").hide();

    document.addEventListener( "touchstart", function () {
    }, false );

    var $body = $( 'body' ), serverUrlPlaceholder = '[[SERVER_URL]]', serverUrl = $body.data( $body.data( 'server-mode' ) + '-server-url' );

    // UI Module
    var UI = (function () {
        return {
            show: function ( $ele, verdict ) {
                verdict ? $ele.removeClass( 'hidden' ) : $ele.addClass( 'hidden' );
                return $ele;
            },
            showMessage: function ( $ele, msg, type ) {
                $ele.addClass( type ).empty().append( msg );
                this.show( $ele, true );
            }
        }
    })();

    var Job = (function () {
        function convertFormToJSON( $form ) {
            var formJson = {};
            $.each( $form.serializeArray(), function () {
                formJson[this.name] = this.value || '';
            } );
            return formJson;
        }

        return {
            submitForm: function ( $form, contentType ) {
                var $msgHolder = $form.children( '.message-holder' ), cType = contentType ? contentType : 'application/json';
                var options = { url: $form.attr( 'action' ).replace( serverUrlPlaceholder, serverUrl ), type: "POST", contentType: cType,
                    data: cType === 'application/x-www-form-urlencoded' ? $form.serialize() : JSON.stringify( convertFormToJSON( $form ) )};
                if ( cType !== 'application/x-www-form-urlencoded' ) {
                    options['dataType'] = 'json';
                }
                UI.show( $msgHolder, false );
                $.ajax( options ).done( function ( data ) {
                    $form.trigger( 'reset' );

                    var isUser = data.data.split(':');
                    if(!isUser[1]){}
                    else {
                        loginUser = isUser[1];
                        console.log("loginuser : " + loginUser);
                        if (loginUser) {
                            console.log("entered");
                            $('#shphun').empty().css("display", "block").append(loginUser).attr("data-icon", "user");
                            $('#hphun').empty().css("display", "block").append(loginUser).attr("data-icon", "user");
                            $('#qphun').empty().toggle().append(loginUser).attr("data-icon", "user");
                            $('#aphun').empty().toggle().append(loginUser).attr("data-icon", "user");
                            $("#hphsub").css("display", "none");
                            $("#hphsib").css("display", "none");
                            $('#qphb').css("display", "none");
                            $('#aphb').css("display", "none");
                        }
                    }
                    if ( data && data.data ) {
                        UI.showMessage( $msgHolder, data.data, data.status === 'OK' ? 'success' : 'error' );
                    }
                    if ( data.pageId ) {
                        if(data.pageId == 'suhp')
                        {
                            questionsPageNumber = 0;
                            answersPageNumber = 0;
                            getQuestions( 'user' );
                            displayUserAnswers();
                        }
                        $.mobile.changePage( 'index.html#' + data.pageId );
                    }
                } ).fail( function ( err ) {
                    $msgHolder.empty().append( err ? err : '' );
                } );
            }
        }
    })();

    $(document).on('vclick', '.suhp', function(e){
        e.preventDefault();
        loadUserHomePage();
    });

    $(document).on('vclick', '.logout', function(e){
        e.preventDefault();
        closeAllSessions();
    });

    function loadUserHomePage(){
        if(userCount == 0){
            $('#shphun').toggle().append(loginUser).attr("data-icon", "user");
            userCount++;
        }
        else {
            $('#shphun').empty().append(loginUser).attr("data-icon", "user");
        }
        $.mobile.changePage( 'index.html#suhp' );
        questionsPageNumber = 0;
        answersPageNumber = 0;
        getQuestions( 'user' );
        displayUserAnswers();
    }

    function closeAllSessions(){
        $.ajax( {
            url: serverUrl + '/user/closeUserSession',
            type: 'DELETE',
            dataType: 'json',
            contentType: 'application/json'
        } ).done( function ( data ) {
            console.log( data );
        }).fail( function ( data ) {
            console.log( data );
            deleteActiveUser();
        } );
    }

    function reloadIt(){
        alert("came");
        location.reload();
    }

    function deleteActiveUser(){
        loginUser = null;
        $("#message").addClass('hidden');
        $.mobile.changePage("index.html#uhp");
        $("#logout_popup").trigger("click");
        $('#shphun').toggle();
        $("#hphsub").toggle();
        $("#hphsib").toggle();
        $("#hphun").css("display","none");
        $('#qphb').toggle();
        $('#qphun').toggle();
        $('#aphb').toggle();
        $('#aphun').toggle();
    }

    // Document readiness
    $( document ).ready( function () {
        initializeSignUpPage();
        initializeSignInPage();
        initializeSearchHandlers();
        noOfQuestions();

        $(document).ajaxStart(function(e){
            e.preventDefault();
            $.mobile.loading( 'show', {text: 'Loading...', textVisible: false, theme: 'b'} );

        });

        $(document).ajaxComplete(function(e){
            e.preventDefault();
            $.mobile.loading( 'hide' );
        });

        connectionStatus = window.navigator.onLine;
        console.log(connectionStatus);

        if(connectionStatus == false){
//            app.ec
//            navigator.app.exitApp();
        }

        $( "#answer-submission-form" ).submit( function (e) {
//        var answer = $.trim( $( '#answerName' ).val() );
            e.preventDefault();
            if ( loginUser == null ) {
                $("#signin_popup").trigger("click");
            }
            else {
                $("#answer-submission-form").trigger('reset');
                submitAnswer();
            }
        } );

        $( "#question-submission-form" ).submit( function (e) {
//        var answer = $.trim( $( '#answerName' ).val() );
            e.preventDefault();
            if ( loginUser == null ) {
                $("#question_signin_popup").trigger("click");
            }
            else {
                $("#question-submission-form").trigger('reset');
                submitQuestion();
            }
        } );

    } );

    // SignUp
    function initializeSignUpPage() {
        var $form = $( '#supcSignUpForm' );
        $form.submit( function ( e ) {
            e.preventDefault();
            Job.submitForm( $form );
        } );
    }

    // SignIn
    function initializeSignInPage() {
        var $form = $( '#sipcSignInForm' );
        $form.submit( function ( e ) {
            e.preventDefault();
            Job.submitForm( $form, 'application/x-www-form-urlencoded' );
        } );
    }

    // Search button initialization
    function initializeSearchHandlers() {
        $(document).on('vclick', '.serch-btn', function(e){
            e.preventDefault();
            $( '.search-questions-form-container' ).toggleClass( 'hidden' );
        } );
    }

    // Number of Questions in each tag
    function noOfQuestions() {
        $.ajax( {
            url: serverUrl + '/questions/count',
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json'
        } ).done( function ( data ) {
            console.log( data );
            var c = 0;
            $( "#browseByCategory li" ).each( function ( e ) {
                var categoryType = $( this ).attr( 'id' );
                var arr = categoryType.split( '_' );
                $( "#" + arr[0] + 'Count' ).empty().append( data.tagSize[c] );
                c++;
            } );
        } ).fail( function ( data ) {
            console.log( data );
        } );

    }

//    Category wise browsing
    $(document).on('vclick', '#browseByCategory li', function(e){
        e.preventDefault();
        $( "#displayQuestions" ).empty();
        var categoryType = $( this ).attr( 'id' );
        questionsPageNumber = 0;
        window.location = serverUrl + "/resources/static/index.html#questionsPage";
        getQuestions( categoryType );
    } );

//    Browse all
    $(document).on('vclick', '#browseAll', function(e){
        $( "#displayQuestions" ).empty();
        e.preventDefault();
        questionsPageNumber = 0;
        window.location = serverUrl + "/resources/static/index.html#questionsPage";
        getQuestions( 3 );
    } );

// Question Submission
    function submitQuestion(){
        var questionData =
        {
            "questionTitle": $("#questionTitle").val(),
            "questionDescription": $("#questionDescription").val(),
            "companyName": $("#companyName").val(),
            "categoryType": $("#categoryType").val()
        }

        $.ajax({
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            url: serverUrl + '/questions/newQuestion',
            data: JSON.stringify(questionData)
        }).done(function (data) {
            console.log(data);
            $("#question-submission-form").trigger('reset');
            questionsPageNumber = 0;
            getQuestions(2);
        }).fail(function (data) {
            console.log(data);
            $.mobile.changePage("index.html#sip");
        });
    }

// Displaying questions
    var questionsPageNumber = 0;
    var browseBy = "";
    var categoryType = "";
    var companyName = "";

    $( document ).ready( function () {
        var i = 0;

        $(document).on('vclick', '#refresh', function(e){
            e.preventDefault();
            questionsPageNumber = 0;
            ( companyName == "" && categoryType == "" ) ? getQuestions( 2 ) : getQuestions( browseBy );
        } );
        $(document).on('vclick', '#refresh-s', function(e){
            e.preventDefault();
            questionsPageNumber = 0;
            answersPageNumber = 0;
            getQuestions( 'user' );
            displayUserAnswers();
        } );
        $(document).on('vclick', '#nextQuestion', function(e){
            e.preventDefault();
            getQuestions( browseBy );
        } );
        $(document).on('vclick', '#previousQuestion', function(e){
            e.preventDefault();
            questionsPageNumber -= 2;
            getQuestions( browseBy );
        } );
        $(document).on('vclick', '#nxtQuestion', function(e){
            e.preventDefault();
//            $.mobile.loading( 'show', {text: 'Loading', textVisible: true, theme: 'c'} );
            getQuestions( 'user' );
        } );
        $(document).on('vclick', '#prvsQuestion', function(e){
            e.preventDefault();
            questionsPageNumber -= 2;
            getQuestions( 'user' );
        } );
    } );

    function getQuestions( reloadType ) {
        var urlQ = "";
        var questionUUIDs = [];
        var totalQuestions = 0;
        var numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/;

        $( "#nextQuestion" ).show();
        $( "#previousQuestion" ).show();
        $( "#nxtQuestion" ).hide();
        $( "#prvsQuestion" ).hide();
        $("#nxtAnswer").hide();
        $("#prvsAnswer").hide();
        if ( !numberRegex.test( reloadType ) ) {
            var arr = reloadType.split( '_' );
            if ( arr[1] == 'c' ) {
                categoryType = "";
                companyName = arr[0];
                urlQ = serverUrl + '/questions?pageNo=' + questionsPageNumber + '&categoryType=' + categoryType + '&companyName=' + companyName;
            }
            else if( reloadType == 'user' )
            {
                $( "#nextQuestion" ).hide();
                $( "#previousQuestion" ).hide();
                $( "#nxtQuestion" ).show();
                $( "#prvsQuestion" ).show();
                $("#nxtAnswer").show();
                $("#prvsAnswer").show();
                urlQ =  serverUrl + '/questions/userQuestions?pageNo=' + questionsPageNumber;
            }
            else {
                companyName = "";
                categoryType = arr[0];
                urlQ = serverUrl + '/questions?pageNo=' + questionsPageNumber + '&categoryType=' + categoryType + '&companyName=' + companyName;
            }
            browseBy = reloadType;
        }
        else {
            categoryType = "";
            companyName = "";
            urlQ = serverUrl + '/questions?pageNo=' + questionsPageNumber + '&categoryType=' + categoryType + '&companyName=' + companyName;
        }

        $.ajax( {
            url: urlQ,
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json'
        } ).done( function ( data ) {
            console.log( data );

            totalQuestions = data.totalQuestions;
            questionsPageNumber = data.pageNum;
            var noOfQuestionsInPage = data.questions.length;

            var html = '';

            if ( noOfQuestionsInPage > 0 ) {
                html += reloadType == 'user' ? '<li data-role="list-divider">Posted Questions</li>' : '<ul id="questionContainer" data-role="listview" class="ui-listview ui-listview-inset ui-corner-all ui-shadow">';
                for ( i = 0; i < noOfQuestionsInPage; i++ ) {
                    html += i == 0 ? '<li class="ui-first-child">' : (i == noOfQuestionsInPage - 1 ? '<li class="ui-last-child">' : '<li>');
                    var questionID = 'question' + i;
                    var time = moment( data.questions[i].postingTime ).format( 'lll' );
                    html += '<a href="#answersPage" class="ui-btn get-answer-for-it" id="' + questionID + '">';
                    html += '<h1>' + data.questions[i].questionTitle + '</h1>';
                    html += '<p class="ui-li-aside">' + time + '</br> Views ' + data.questions[i].noOfViews + '</p>';
                    html += '<p class="ui-li-count">' + data.questions[i].noOfAnswers + '</p>';
                    html += '<p>' + data.questions[i].questionDescription + '</p>';
                    html += reloadType == 'user' ? '<p>' + data.questions[i].companyName + ' - ' + data.questions[i].categoryType + '</p>' : '<p> by ' + data.users[i].displayName + ' - ' + data.questions[i].companyName + ' - ' + data.questions[i].categoryType + '</p>';
                    html += '</a></li>';
                    questionUUIDs[i] = data.questions[i].objectUUID;
                }
            }
            else {
                html += reloadType == 'user' ? '<p align="center">Sorry you are not posted any questions.. <a href="#sqp">POST NOW</a></p>' : '</ul><p align="center">Sorry there are no questions posted in this category.. Be the FIRST ONE</p>';
            }

            if( reloadType == 'user' ) {
                $( "#questionsPosted" ).empty().append( html );

                if(totalQuestions == 0)
                {
                    $("#qtnBtnsList").hide();
                }
                else
                {
                    $("#qtnBtnsList").show();
                    if (((questionsPageNumber + 1) * 3) >= totalQuestions) {
                        $("#nxtQuestion").attr('disabled', 'disabled');
                    }
                    else {
                        $("#nxtQuestion").removeAttr('disabled', 'disabled');
                    }

                    questionsPageNumber++;

                    if (questionsPageNumber > 1 && totalQuestions > 3) {
                        $("#prvsQuestion").removeAttr('disabled', 'disabled');
                    }
                    else {
                        $("#prvsQuestion").attr('disabled', 'disabled');
                    }
                }
            }
            else {
                $("#displayQuestions").empty().append(html);

                if (((questionsPageNumber + 1) * 10) >= totalQuestions) {
                    $("#nextQuestion").attr('disabled', 'disabled');
                }
                else {
                    $("#nextQuestion").removeAttr('disabled', 'disabled');
                }

                questionsPageNumber++;

                if (questionsPageNumber > 1 && totalQuestions > 10) {
                    $("#previousQuestion").removeAttr('disabled', 'disabled');
                }
                else {
                    $("#previousQuestion").attr('disabled', 'disabled');
                }
            }
            questionLinkListener( questionUUIDs );
        } ).fail( function ( data ) {
            alert( "error" );
            console.log( data );
        } );
    }

    function questionLinkListener( questionUUID ) {
//        var linkType = (browseBy == 'user' ? "#questionsPosted a" : browseBy == 'answer' ? "#answersSubmitted a" : "#questionContainer a");
//        $(document).on('vclick', ''+linkType+'', function(e){
        $( browseBy == 'user' ? "#questionsPosted a" : browseBy == 'answer' ? "#answersSubmitted a" : "#questionContainer a" ).click( function ( e ) {
            var questionId = $( this ).attr( 'id' );
            var arr = questionId.split( 'n' );
            answersPageNumber = 0;
            getAnswers( questionUUID[arr[1]] );
        } );
    }

// Displaying answers and its related functions
    var answersPageNumber = 0;
    var loadedQuestion;

/*    $( "#answer-submission-form" ).submit( function () {
//        var answer = $.trim( $( '#answerName' ).val() );
        if ( loginUser == null ) {
            $("#signin_popup").trigger("click");
        }
        else {
            $("#answer-submission-form").trigger('reset');
            submitAnswer();
        }
    } );*/


    $(document).on('vclick', '#refresh-a', function(e){
        e.preventDefault();
        answersPageNumber = 0;
        getAnswers( loadedQuestion );
    } );
    $(document).on('vclick', '#nextAnswer', function(e){
        e.preventDefault();
        getAnswers( loadedQuestion );
    } );
    $(document).on('vclick', '#previousAnswer', function(e){
        e.preventDefault();
        answersPageNumber -= 2;
        getAnswers( loadedQuestion );
    } );
    $(document).on('vclick', '#nxtAnswer', function(e){
        e.preventDefault();
        displayUserAnswers();
    } );
    $(document).on('vclick', '#prvsAnswer', function(e){
        e.preventDefault();
        answersPageNumber -= 2;
        displayUserAnswers();
    } );

    function submitAnswer() {
        var answerData =
        {
            "answerName": $( "#answerName" ).val()
        }
        $.ajax( {
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            url: serverUrl + '/questions/' + loadedQuestion + '/newAnswer',
            data: JSON.stringify( answerData )
        } ).done( function ( data ) {
//            $.mobile.loading( 'hide' );
            $( "#answer-submission-form" ).trigger( 'reset' );
            if(data.isSubmit=="false")
            {
                alert("You already submitted the answer for this Question");
                $.mobile.changePage(serverUrl + "index.html#answersPage");
                getAnswers( loadedQuestion );
            }
            else {
                $.mobile.changePage(serverUrl + "index.html#answersPage");
                answersPageNumber = 0;
                getAnswers( loadedQuestion );
            }
            console.log( data );
        } ).fail( function ( data ) {
            $.mobile.changePage( serverUrl + "index.html#sip" );
            console.log( data );
        } );
    }


    function displayUserAnswers(){
        var totalAnswers;
        var answerUUIDs = [];

        $( "#nextQuestion" ).hide();
        $( "#previousQuestion" ).hide();
        $( "#nxtQuestion" ).show();
        $( "#prvsQuestion" ).show();
        $("#nxtAnswer").show();
        $("#prvsAnswer").show();

        $.ajax( {
            url: serverUrl + '/questions/userAnswers?pageNumber=' + answersPageNumber,
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json'
        } ).done( function ( data ) {
            console.log( data );
            totalAnswers = data.totalAnswers;
            answersPageNumber = data.pageNumber;
            var noOfAnswersInPage = data.answers.length;

            var html = '';

            if( noOfAnswersInPage>0 ){
                html += '<li data-role="list-divider">Submitted Answers</li>';
                for ( i = 0; i < noOfAnswersInPage; i++ ) {
                    html += i == 0 ? '<li class="ui-first-child">' : (i == noOfAnswersInPage - 1 ? '<li class="ui-last-child">' : '<li>');
                    var answerID = 'question' + i;
                    var time = moment( data.answers[i].currentTime ).format( 'lll' );
                    html += '<a href="#answersPage" class="ui-btn get-answer-for-it" id="' + answerID + '">';
                    html += '<h1>' + data.answers[i].answerName + ' FOR </h1>';
                    html += '<p class="ui-li-aside">' + time + '</p>';
                    html += '<p>' + data.questions[i].questionTitle + '</p>';
                    html += '</a></li>';
                    answerUUIDs[i] = data.questions[i].objectUUID;
                }
            }

            if(totalAnswers == 0)
            {
                html += '<p align="center"> You are not answered any Question.. <a href="#questionsPage">Answer NOW</a></p>';
                $( "#answersSubmitted" ).empty().append( html );
                $("#ansBtnsList").hide();
//                $("#prvsAnswer").hide();
            }
            else {
                $("#ansBtnsList").show();
                $( "#answersSubmitted" ).empty().append( html );

                if (((answersPageNumber + 1) * 3) >= totalAnswers) {
                    $("#nxtAnswer").attr('disabled', 'disabled');
                }
                else {
                    $("#nxtAnswer").removeAttr('disabled', 'disabled');
                }

                answersPageNumber++;

                if (answersPageNumber > 1 && totalAnswers > 3) {
                    $("#prvsAnswer").removeAttr('disabled', 'disabled');
                }
                else {
                    $("#prvsAnswer").attr('disabled', 'disabled');
                }
            }

            browseBy = 'answer';
            questionLinkListener( answerUUIDs );

        }).fail( function( data ) {
            console.log( data );
        });
    }

    function getAnswers( questionsUUID ) {
        var totalAnswers = 0;
        var answerUUIDs = [];

        loadedQuestion = questionsUUID;
        $.ajax( {
            url: serverUrl + '/questions/' + loadedQuestion + '?pageNumber=' + answersPageNumber,
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json'
        } ).done( function ( data ) {
            totalAnswers = data.totalAnswers;
            answersPageNumber = data.pageNumber;
            var noOfAnswersInPage = data.answers.length;

            var html = '';
            var q = '';
            q += ''+data.question.questionDescription + '<p align="right">by ' + data.userOfQuestion + ' - ' + data.question.companyName + ' - ' + data.question.categoryType + '</p>';
            $("#questionDetails").empty().append(q);
            if ( noOfAnswersInPage > 0 ) {
                var userOfComment = 0;
                for ( var i = 0; i < noOfAnswersInPage; i++ ) {
                    var time = moment( data.answers[i].currentTime ).format( 'lll' );
                    var likes = 'likes' + i;
                    html += '<li data-theme="c" data-role="list-divider">' + time + '<span id="' + likes + '" class="ui-li-count">' + ((data.answers[i].likes) - (data.answers[i].disLikes)) + '</span></li>';
                    html += i == 0 ? '<li class="ui-first-child">' : (i == noOfAnswersInPage - 1 ? '<li class="ui-last-child">' : '<li>');
                    var answerUID = 'answerupq' + i;
                    var answerDID = 'answerdownq' + i;
                    answerUUIDs[i] = data.answers[i].objectUUID;
                    html += '<a class="ui-btn">';
                    html += '<p class="has-vote-btns" style="white-space: normal"><strong>' + data.answers[i].answerName + '</strong></p>';
                    html += '<p class="has-vote-btns"> by ' + data.users[i].displayName + '</p>';
                    html += '<div class="vote-btns ui-alt-icon">';
                    html += '<a id="' + answerUID + '" class="answerTool ui-btn ui-shadow ui-corner-all ui-icon-thumbs-o-up ui-btn-icon-notext">Up</a>';
                    html += '<a id="' + answerDID + '" class="answerTool ui-btn ui-shadow ui-corner-all ui-icon-thumbs-o-down ui-btn-icon-notext">Down</a>';
                    html += '</div></a>';
                    html += '</li>';
                    var commentId = 'comment' + i;
                    var commentsId = 'comments' + i;
                    var formId = 'form' + i;
                    var noOfComments = data.comments[i].length;
                    for ( var m = 0; m < noOfComments; m++ ) {
                        html += '<li align="right">' + data.comments[i][m].commentDescription + ' <h7 style="font-size: 10px; color: blue">by ' + data.usersOfComments[userOfComment] + '</h7></li>';
                        userOfComment++;
                    }
                    html += '<div class="commentIDs">';
                    html += '<li align="center" data-mini="true" data-inline="true" id="' + commentId + '">Add comment</li>';
                    html += '<li align="center"><form id="' + formId + '"  style="display:none"><lable for="' + commentsId + '"></lable><input data-inline="true" data-mini="true" required="required" type="text" id="' + commentsId + '"/><label for="submit"></label><input type="submit" id="submit" value="Comment" data-mini="true" data-inline="true"/></form></li>';
                    html += '</div>';
                }
            }
            $( "#answerContainer").empty().append( html);
            $("#answerContainer").listview("refresh");
            $(".commentIDs").trigger("create");

            if ( ((answersPageNumber + 1) * 10) >= totalAnswers ) {
                $( "#nextAnswer" ).attr( 'disabled', 'disabled' );
            }
            else {
                $( "#nextAnswer" ).removeAttr( 'disabled', 'disabled' );
            }

            answersPageNumber++;

            if ( answersPageNumber > 1 && totalAnswers > 10 ) {
                $( "#previousAnswer" ).removeAttr( 'disabled', 'disabled' );
            }
            else {
                $( "#previousAnswer" ).attr( 'disabled', 'disabled' );
            }

            console.log( data );
            answerLinkListener( answerUUIDs, loadedQuestion );
        } ).fail( function ( data ) {
            alert( "error" );
        } );
    }

    // Binds all the answer related events
    function answerLinkListener( aUUIDs, loadedQtn ) {
        // Vote up and down for each answer
        $(document).on('vclick', '#answerContainer a', function(e){
//            e.preventDefault();
            if($(this).hasClass('answerTool')) {
                if (loginUser == null) {
                    $("#signin_popup").trigger("click");
                }
                else {
                    var typeOfUpdate = $(this).attr('id');
                    var arr = typeOfUpdate.split('r');
                    var arr2 = arr[1].split('q');

                    if (arr2[0] == 'up') {
                        voteUp(loadedQtn, aUUIDs[arr2[1]], arr2[1]);
                    }
                    else {
                        voteDown(loadedQtn, aUUIDs[arr2[1]], arr2[1]);
                    }
                }
            }
        } );

        // Toggles add comment input box for each answer\
        $(".commentIDs li:first-child").click(function(e){
            e.preventDefault();
            var answerId = $( this ).attr( 'id' );
            var arr = answerId.split( 't' );
            $( "#form" + arr[1] ).toggle();
        } );

        // Submit the given comment for the specific answer
        $(".commentIDs form").submit(function(e){
            e.preventDefault();
            if ( loginUser == null ) {
                $("#signin_popup").trigger("click");
            }
            else {
                var answerId = $( this ).attr( 'id' );
                var arr = answerId.split( 'm' );
                addComment( loadedQtn, aUUIDs[arr[1]], arr[1] );
            }
        } );
    }

    function voteUp( questionUID, answerUID, likesUID ) {
        $.ajax( {
            url: serverUrl + '/questions/' + questionUID + '/' + answerUID + '/voteUp',
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json'
        } ).done( function ( data ) {
            console.log( data );
            $( "#likes" + likesUID ).empty().append( data.noOfLikes );
        } );
    }

    function voteDown( questionUID, answerUID, likesUID ) {
        $.ajax( {
            url: serverUrl + '/questions/' + questionUID + '/' + answerUID + '/voteDown',
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json'
        } ).done( function ( data ) {
            console.log( data );
            $( "#likes" + likesUID ).empty().append( data.noOfLikes );
        } );
    }

    function addComment( questionUID, answerUID, commentsID ) {
        var commentData =
        {
            "commentDescription": $( "#comments" + commentsID ).val()
        }

        $.ajax( {
            url: serverUrl + '/questions/' + questionUID + '/' + answerUID + '/addComment',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify( commentData )
        } ).done( function ( data ) {
            console.log( data );
            getAnswers( loadedQuestion );
//            $("#answerContainer").listview("refresh");
            $.mobile.reloadPage();
        } ).fail( function ( data ) {
            $.mobile.changePage( serverUrl + "/resources/static/index.html#sip" );
            console.log( data );
        } );
    }

// Displaying search results and its related functions
    $(document).on('vclick', '#search', function(e){
        e.preventDefault();
        $( "#search-area" ).toggle();
    } );
    $(document).on('vclick', '#search-a', function(e){
        e.preventDefault();
        $( "#search-area-a" ).toggle();
    } );
    $(document).on('vclick', '#search-s', function(e){
        e.preventDefault();
        $( "#search-area-s" ).toggle();
    } );

    var searchPageNo = 0;
    var totalResults = 0;
    var s;

    /*
     $( ".search-questions-form" ).submit( function ( e ) {
     e.preventDefault();
     $( '.search-questions-form-container' ).toggleClass( 'hidden' );
     searchPageNo = 0;
     getResults( 1 );
     } );
     */

    $( "#search-area" ).submit( function ( e ) {
        e.preventDefault();
        $("#search").trigger('click');
        searchPageNo = 0;
        getResults( 1 );
    } );
    $( "#search-area-a" ).submit( function ( e ) {
        e.preventDefault();
        $("#search-a").trigger('click');
        searchPageNo = 0;
        getResults( 3 );
    } );

    $( "#search-area-s" ).submit( function ( e ) {
        e.preventDefault();
        $("#search-s").trigger('click');
        searchPageNo = 0;
        getResults( 2 );
    } );

    function getResults( type ) {
        var questionUUID = [], searchText;

/*        $text = $(".search-form").find(".search-text");
        $text.each(function(){
            if($.trim( $( '.search-text' ).val() )){
                searchText = $.trim( $( '.search-text' ).val() );
                alert(searchText);
            }
        });
        alert(searchText);*/


        if ( type == 1 ) {
            searchText = $.trim( $( '#searchText' ).val() );
            $("#search-area").trigger('reset');
        }
        else if( type == 2 ) {
            searchText = $.trim($('#searchText-s').val());
            $("#search-area-a").trigger('reset');
            $.mobile.changePage( serverUrl + "/resources/static/index.html#questionsPage" );
        }
        else {
            searchText = $.trim( $( '#searchText-a' ).val() );
            $("#search-area-s").trigger('reset');
            $.mobile.changePage( serverUrl + "/resources/static/index.html#questionsPage" );
        }
        console.log(searchText);

        $.ajax( {
            url: serverUrl + '/search?searchText=' + searchText + '&pageNo=' + searchPageNo,
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json'
        } ).done( function ( data ) {

            totalResults = data.totalResults;
            searchPageNo = data.pageNum;

            var pageSize = data.results.length;

            $( "#displayQuestions" ).empty();

            var html = '';

            if ( pageSize > 0 ) {
                html += '<ul id="resultContainer" data-role="listview" class="ui-listview ui-listview-inset ui-corner-all ui-shadow">';
                for ( i = 0; i < pageSize; i++ ) {
                    html += i == 0 ? '<li class="ui-first-child">' : (i == pageSize - 1 ? '<li class="ui-last-child">' : '<li>');
                    var questionID = 'question' + i;
                    var time = moment( data.results[i].postingTime ).format( 'lll' );
//                    html += '<div id="questionContainer">';
                    html += '<a href="#answersPage" class="ui-btn get-answer-for-it" id="' + questionID + '">';
                    html += '<h1>' + data.results[i].questionTitle + '</h1>';
                    html += '<p class="ui-li-aside">' + time + '</br> Views ' + data.results[i].noOfViews + '</p>';
                    html += '<p class="ui-li-count">' + data.results[i].noOfAnswers + '</p>';
                    html += '<p>' + data.results[i].questionDescription + '</p>';
                    html += '<p> by ' + data.users[i].displayName + ' - ' + data.results[i].companyName + ' - ' + data.results[i].categoryType + '</p>';
                    html += '</a></li>';
                    questionUUID[i] = data.results[i].objectUUID;
                }
                html += '</ul>';
                $( "#displayQuestions" ).append( html );
            }

            if ( ((searchPageNo + 1) * 10) >= totalResults ) {
                $( "#nextQuestion" ).attr( 'disabled', 'disabled' );
            }
            else {
                $( "#nextQuestion" ).removeAttr( 'disabled', 'disabled' );
            }

            searchPageNo++;

            if ( searchPageNo > 1 && totalResults > 10 ) {
                $( "#previousQuestion" ).removeAttr( 'disabled', 'disabled' );
            }
            else {
                $( "#previousQuestion" ).attr( 'disabled', 'disabled' );
            }

            console.log( data );
            resultLinkListener( questionUUID, 2 );

        } ).fail( function ( data ) {
            alert( "error" );
            console.log( data );
        } );
    }

    function resultLinkListener( questionUUID, reload ) {
        $(document).on('vclick', '#resultContainer a', function(e){
            var questionId = $( this ).attr( 'id' );
            var arr = questionId.split( 'n' );
            answersPageNumber = 0;
            getAnswers( questionUUID[arr[1]] );
        } );
    }

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "js/facebook/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    $(function () {
        $(document).on('vclick', '#login', function(){
            $.fblogin({
                fbId: '1505474179706692',
                permissions: 'email',
                // fields: 'first_name,last_name,locale,email,birthday',
                success: function (data) {
                    console.log('done everything', data);
                    console.log('User email:'+data.email);
                    checkUser(data);
                }
            });
        });

        function checkUser(data) {
            var userData =
            {
                "displayName": data.name,
                "email": data.email,
                "firstName": data.first_name,
                "lastName": data.last_name,
                "gender": data.gender
            }
            $.ajax( {
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                url: serverUrl + '/user/fbuser',
                data:  JSON.stringify( userData )
            } ).done( function ( data ) {
                console.log('done successfully'+data.pageID);
                loginUser = data.username;
                loadUserHomePage();
            } ).fail( function ( data ) {
                console.log( data );
            } );
        }
    });


})( jQuery );
